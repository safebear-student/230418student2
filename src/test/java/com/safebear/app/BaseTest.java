package com.safebear.app;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    WebDriver driver;

    @Before
    public void setUp() {

        driver = new ChromeDriver();

        driver.get("http://automate.safebear.co.uk/");

    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    driver.manage().window().maximize();

    }

    @After
    public void tearDown() {
    driver.quit();
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }



}